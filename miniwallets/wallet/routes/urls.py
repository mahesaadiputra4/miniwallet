from django.conf.urls import url
from django.urls import path,include
from ..view import CreateWalletAPIView,EnabledWalletAPIView,DepositsWalletAPIView,WithdrawalsWalletAPIView


urlwallet = [
    path('/init', CreateWalletAPIView.as_view()),
    path('/wallet', EnabledWalletAPIView.as_view()),
    path('/wallet/deposits', DepositsWalletAPIView.as_view()),
    path('/wallet/withdrawals', WithdrawalsWalletAPIView.as_view()),
]