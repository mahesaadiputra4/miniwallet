# Generated by Django 3.2.7 on 2021-09-29 16:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0003_referenceid'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallets',
            name='deposited_at',
            field=models.DateTimeField(null=True),
        ),
    ]
