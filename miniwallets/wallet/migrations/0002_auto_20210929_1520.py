# Generated by Django 3.2.7 on 2021-09-29 15:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallets',
            name='amount',
            field=models.FloatField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='wallets',
            name='balance',
            field=models.FloatField(default=0, null=True),
        ),
    ]
