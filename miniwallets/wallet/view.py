import sys

sys.path.append("../..")
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from .serializers import WalletSerializer,ReferenceSerializer
from rest_framework import status
from rest_framework.response import Response
from ..settings import SECRET_KEY
import jwt
from datetime import datetime
from .models import wallets,referenceId
from rest_framework_jwt.settings import api_settings
from django.core.exceptions import ValidationError


class CreateWalletAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        # wallet = request.data
        # serializer = WalletSerializer(data=wallet)
        # serializer.is_valid(raise_exception=True)
        wallet = wallets(id=request.data['customer_xid'])
        wallet.save()
        try:
            data_payload = {"id": request.data['customer_xid']}
            token = jwt.encode(data_payload, SECRET_KEY)
            response = {"data": {"token": token},
                        "status": "success"}
        except Exception as e:
            raise e

        return Response(response, status=status.HTTP_201_CREATED)


class EnabledWalletAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        bearer = str(token).split(" ")
        data = jwt.decode(bearer[1], SECRET_KEY)
        wallets.objects.filter(id=data['id']).update(status="enabled", owned_by=data['id'])
        final_data = wallets.objects.get(id=data['id'])
        response = {
            "status": "success",
            "data": {
                "wallet": {
                    "id": final_data.id,
                    "owned_by": final_data.owned_by,
                    "status": final_data.status,
                    "enabled_at": final_data.enabled_at,
                    "balance": final_data.balance
                }
            }
        }

        return Response(response, status=status.HTTP_200_OK)

    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        bearer = str(token).split(" ")
        data = jwt.decode(bearer[1], SECRET_KEY)
        final_data = wallets.objects.get(id=data['id'])
        if not final_data or final_data.status != "enabled":
            raise ValidationError("The wallet is not active")

        response = {
            "status": "success",
            "data": {
                "wallet": {
                    "id": final_data.id,
                    "owned_by": final_data.owned_by,
                    "status": final_data.status,
                    "enabled_at": final_data.enabled_at,
                    "balance": final_data.balance
                }
            }
        }

        return Response(response, status=status.HTTP_200_OK)

    def patch(self,request):
        token = request.META.get('HTTP_AUTHORIZATION')
        bearer = str(token).split(" ")
        data = jwt.decode(bearer[1], SECRET_KEY)

        if request.data['is_disabled'] == True:
            wallets.objects.filter(id=data['id']).update(status="disabled",disabled_at=datetime.now())
        final_data = wallets.objects.get(id=data['id'])

        response = {
            "status": "success",
            "data": {
                "wallet": {
                    "id": final_data.id,
                    "owned_by": final_data.owned_by,
                    "status": final_data.status,
                    "disabled_at": datetime.now(),
                    "amount": final_data.amount
                }
            }
        }

        return Response(response, status=status.HTTP_200_OK)



class DepositsWalletAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        reference = request.data
        serializer = ReferenceSerializer(data=reference)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        token = request.META.get('HTTP_AUTHORIZATION')
        bearer = str(token).split(" ")
        data = jwt.decode(bearer[1], SECRET_KEY)
        final_data = wallets.objects.get(id=data['id'])
        if not final_data or final_data.status != "enabled":
            raise ValidationError("The wallet is not active")
        wallets.objects.filter(id=data['id']).update(amount=final_data.amount + float(request.data['amount']),
                                                     reference_id=request.data['reference_id'],deposited_at=datetime.now())

        response = {
            "status": "success",
            "data": {
                "wallet": {
                    "id": final_data.id,
                    "deposited_by": final_data.owned_by,
                    "status": "success",
                    "deposited_at": datetime.now(),
                    "amount": final_data.amount + float(request.data['amount']),
                    "reference_id": request.data['reference_id']
                }
            }
        }

        return Response(response, status=status.HTTP_200_OK)


class WithdrawalsWalletAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        reference = request.data
        serializer = ReferenceSerializer(data=reference)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        token = request.META.get('HTTP_AUTHORIZATION')
        bearer = str(token).split(" ")
        data = jwt.decode(bearer[1], SECRET_KEY)
        final_data = wallets.objects.get(id=data['id'])
        if not final_data or final_data.status != "enabled":
            raise ValidationError("The wallet is not active")
        if final_data.amount - float(request.data['amount']) < 0:
            raise ValidationError(f"Can't withdraw with Amount {float(request.data['amount'])}")
        wallets.objects.filter(id=data['id']).update(amount=final_data.amount - float(request.data['amount']))
        response = {
            "status": "success",
            "data": {
                "wallet": {
                    "id": final_data.id,
                    "withdrawn_by": final_data.owned_by,
                    "status": "success",
                    "enabled_at": final_data.enabled_at,
                    "amount": final_data.amount - float(request.data['amount']),
                    "reference_id": request.data['reference_id']
                }
            }
        }

        return Response(response, status=status.HTTP_200_OK)
