from __future__ import unicode_literals
from django.db import models
from django.utils import timezone


class wallets(models.Model):
    id = models.CharField(max_length=40, unique=True,primary_key=True)
    owned_by = models.CharField(max_length=30, blank=True)
    status = models.CharField(max_length=30, blank=True)
    withdrawn_by = models.CharField(max_length=30, blank=True)
    reference_id = models.CharField(max_length=30, blank=True)
    enabled_at = models.DateTimeField(default=timezone.now)
    withdrawn_at = models.DateTimeField(null=True)
    deposited_at = models.DateTimeField(null=True)
    disabled_at = models.DateTimeField(null=True)
    balance = models.FloatField(null=True,default=0)
    amount = models.FloatField(null=True,default=0)
    state = models.CharField(max_length=30,blank=True)

    REQUIRED_FIELDS = ['id']

    def save(self, *args, **kwargs):
        super(wallets, self).save(*args, **kwargs)
        return self

class referenceId(models.Model):
    reference_id = models.CharField(max_length=30,unique=True)
    REQUIRED_FIELDS = ['reference_id']


    def save(self, *args, **kwargs):
        super(referenceId, self).save(*args, **kwargs)
        return self