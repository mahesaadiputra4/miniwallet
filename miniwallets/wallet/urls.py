from django.conf.urls import url
from django.urls import path,include
from .routes.urls import urlwallet


urlpatterns = [
    path('v1', include(urlwallet)),
]