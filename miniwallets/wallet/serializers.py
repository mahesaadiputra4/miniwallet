from rest_framework import serializers
from .models import wallets,referenceId


class WalletSerializer(serializers.ModelSerializer):
    enabled_at = serializers.ReadOnlyField()
    balance = serializers.ReadOnlyField()
    class Meta(object):
        model = wallets
        fields = ('id','owned_by','status','enabled_at','balance','state')


class ReferenceSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = referenceId
        fields = ('reference_id',)